#!/bin/bash

set -o nounset
set -o pipefail
set -e

curl https://sh.rustup.rs -sSf | sh -s -- -y
. "$HOME/.cargo/env"

rustup override set nightly

rustup target add wasm32-unknown-unknown

cargo install wasm-bindgen-cli

cargo build --release --bin server --no-default-features --target wasm32-unknown-unknown --features ssr
cargo build --release --bin client --no-default-features --target wasm32-unknown-unknown --features hydrate

wasm-bindgen target/wasm32-unknown-unknown/release/server.wasm --out-name index --no-typescript --target bundler --out-dir site
wasm-bindgen target/wasm32-unknown-unknown/release/client.wasm --out-name index --no-typescript --target web --out-dir site/pkg