use std::convert::TryInto;
use std::str::FromStr;
use std::fmt;
use std::fmt::{Display, Formatter, Result};
use std::ffi::c_ulonglong;

static COLS: [char; 8] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

static WHITE_PAWNS: u64 = 0xFF00;
static WHITE_KNIGHTS: u64 = 0x42;
static WHITE_BISHOPS: u64 = 0x24;
static WHITE_ROOKS: u64 = 0x81;
static WHITE_QUEENS: u64 = 0x10;
static WHITE_KING: u64 = 0x8;

static BLACK_PAWNS: u64 = 0xFF00000000000000;
static BLACK_KNIGHTS: u64 = 0x4200000000000000;
static BLACK_BISHOPS: u64 = 0x2400000000000000;
static BLACK_ROOKS: u64 = 0x8100000000000000;
static BLACK_QUEENS: u64 = 0x1000000000000000;
static BLACK_KING: u64 = 0x800000000000000;

macro_rules! soft_assert {
    ($expr:expr) => {
        if !$expr {
            return false;
        }
    };
}

static KING_MOVES: [u64; 64] = [
	0x302, 0x705, 0xe0a, 0x1c14,
	0x3828, 0x7050, 0xe0a0, 0xc040,
	0x30203, 0x70507, 0xe0a0e, 0x1c141c,
	0x382838, 0x705070, 0xe0a0e0, 0xc040c0,
	0x3020300, 0x7050700, 0xe0a0e00, 0x1c141c00,
	0x38283800, 0x70507000, 0xe0a0e000, 0xc040c000,
	0x302030000, 0x705070000, 0xe0a0e0000, 0x1c141c0000,
	0x3828380000, 0x7050700000, 0xe0a0e00000, 0xc040c00000,
	0x30203000000, 0x70507000000, 0xe0a0e000000, 0x1c141c000000,
	0x382838000000, 0x705070000000, 0xe0a0e0000000, 0xc040c0000000,
	0x3020300000000, 0x7050700000000, 0xe0a0e00000000, 0x1c141c00000000,
	0x38283800000000, 0x70507000000000, 0xe0a0e000000000, 0xc040c000000000,
	0x302030000000000, 0x705070000000000, 0xe0a0e0000000000, 0x1c141c0000000000,
	0x3828380000000000, 0x7050700000000000, 0xe0a0e00000000000, 0xc040c00000000000,
	0x203000000000000, 0x507000000000000, 0xa0e000000000000, 0x141c000000000000,
	0x2838000000000000, 0x5070000000000000, 0xa0e0000000000000, 0x40c0000000000000,
];

static KNIGHT_MOVES: [u64; 64] = [
	0x20400, 0x50800, 0xa1100, 0x142200,
	0x284400, 0x508800, 0xa01000, 0x402000,
	0x2040004, 0x5080008, 0xa110011, 0x14220022,
	0x28440044, 0x50880088, 0xa0100010, 0x40200020,
	0x204000402, 0x508000805, 0xa1100110a, 0x1422002214,
	0x2844004428, 0x5088008850, 0xa0100010a0, 0x4020002040,
	0x20400040200, 0x50800080500, 0xa1100110a00, 0x142200221400,
	0x284400442800, 0x508800885000, 0xa0100010a000, 0x402000204000,
	0x2040004020000, 0x5080008050000, 0xa1100110a0000, 0x14220022140000,
	0x28440044280000, 0x50880088500000, 0xa0100010a00000, 0x40200020400000,
	0x204000402000000, 0x508000805000000, 0xa1100110a000000, 0x1422002214000000,
	0x2844004428000000, 0x5088008850000000, 0xa0100010a0000000, 0x4020002040000000,
	0x400040200000000, 0x800080500000000, 0x1100110a00000000, 0x2200221400000000,
	0x4400442800000000, 0x8800885000000000, 0x100010a000000000, 0x2000204000000000,
	0x4020000000000, 0x8050000000000, 0x110a0000000000, 0x22140000000000,
	0x44280000000000, 0x0088500000000000, 0x0010a00000000000, 0x20400000000000
];

static WHITE_PAWN_MOVES: [u64; 64] = [
	0x200, 0x500, 0xa00, 0x1400,
	0x2800, 0x5000, 0xa000, 0x4000,
	0x20000, 0x50000, 0xa0000, 0x140000,
	0x280000, 0x500000, 0xa00000, 0x400000,
	0x2000000, 0x5000000, 0xa000000, 0x14000000,
	0x28000000, 0x50000000, 0xa0000000, 0x40000000,
	0x200000000, 0x500000000, 0xa00000000, 0x1400000000,
	0x2800000000, 0x5000000000, 0xa000000000, 0x4000000000,
	0x20000000000, 0x50000000000, 0xa0000000000, 0x140000000000,
	0x280000000000, 0x500000000000, 0xa00000000000, 0x400000000000,
	0x2000000000000, 0x5000000000000, 0xa000000000000, 0x14000000000000,
	0x28000000000000, 0x50000000000000, 0xa0000000000000, 0x40000000000000,
	0x200000000000000, 0x500000000000000, 0xa00000000000000, 0x1400000000000000,
	0x2800000000000000, 0x5000000000000000, 0xa000000000000000, 0x4000000000000000,
	0x0, 0x0, 0x0, 0x0,
	0x0, 0x0, 0x0, 0x0
];

static BLACK_PAWN_MOVES: [u64; 64] = [
	0x0, 0x0, 0x0, 0x0,
	0x0, 0x0, 0x0, 0x0,
	0x2, 0x5, 0xa, 0x14,
	0x28, 0x50, 0xa0, 0x40,
	0x200, 0x500, 0xa00, 0x1400,
	0x2800, 0x5000, 0xa000, 0x4000,
	0x20000, 0x50000, 0xa0000, 0x140000,
	0x280000, 0x500000, 0xa00000, 0x400000,
	0x2000000, 0x5000000, 0xa000000, 0x14000000,
	0x28000000, 0x50000000, 0xa0000000, 0x40000000,
	0x200000000, 0x500000000, 0xa00000000, 0x1400000000,
	0x2800000000, 0x5000000000, 0xa000000000, 0x4000000000,
	0x20000000000, 0x50000000000, 0xa0000000000, 0x140000000000,
	0x280000000000, 0x500000000000, 0xa00000000000, 0x400000000000,
	0x2000000000000, 0x5000000000000, 0xa000000000000, 0x14000000000000,
	0x28000000000000, 0x50000000000000, 0xa0000000000000, 0x40000000000000
];

static PAWN_MOVES: [[u64; 64]; 2] = [ WHITE_PAWN_MOVES, BLACK_PAWN_MOVES ];

static START_POST: &str = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
static Pawn: usize = 0;
static Knight: usize = 1;
static Bishop: usize = 2;
static Rook: usize = 3;
static Queen: usize = 4;
static King: usize = 5;
static Chars: [char; 14] = ['p', 'b', 'b', 'r', 'q', 'k', ' ', ' ', 'P', 'N', 'B', 'R', 'Q', 'K'];

pub struct Bitboard {
	pub color_pieces: [u64; 2],
	pub pieces: [u64; 6],
	pub white_turn: bool,
	pub white_king_castle: bool,
	pub white_queen_castle: bool,
	pub black_king_castle: bool,
	pub black_queen_castle: bool,
	pub en_passant: u64,
	pub halfmove_clock: u8,
	pub fullmove_number: u16,
}

impl Bitboard {
	pub fn empty() -> Self {
        Self {
			color_pieces: [0, 0],
			pieces: [0, 0, 0, 0, 0, 0],
			white_turn: false,
			white_king_castle: false,
			white_queen_castle: false,
			black_king_castle: false,
			black_queen_castle: false,
			en_passant: 0,
			halfmove_clock: 0,
			fullmove_number: 0
        }
    }

	pub fn from_fen(fen: &str) -> Self {
		let mut board = Self::empty();
		let mut initial: u64 = 0x8000000000000000;
		println!("{}", fen);
		let arrays = fen.split(' ').collect::<Vec<&str>>();
		for token in arrays[0].chars() {
			if token == ' ' {
				break;
			}
			if token == '/' {
				continue;
			}
			if token.is_digit(10) {
				initial >>= token.to_digit(10).unwrap();
				continue;
			}
			if token.is_uppercase() {
				board.color_pieces[0] |= initial;
			}
			else {
				board.color_pieces[1] |= initial;
			}
			match token.to_ascii_lowercase() {
				'p' => board.pieces[Pawn] |= initial,
				'n' => board.pieces[Knight] |= initial,
				'b' => board.pieces[Bishop] |= initial,
				'r' => board.pieces[Rook] |= initial,
				'q' => board.pieces[Queen] |= initial,
				'k' => board.pieces[King] |= initial,
				_ => panic!("Invalid character in FEN string pieces"),
			}
			initial >>= 1
		}
		board.white_turn = arrays[1] == "w";
		for token in arrays[2].chars() {
			if token == ' ' {
				break;
			}
			match token {
				'K' => board.white_king_castle = true,
				'Q' => board.white_queen_castle = true,
				'k' => board.black_king_castle = true,
				'q' => board.black_queen_castle = true,
				'-' => break,
				_ => panic!("Invalid character in FEN string castling with character {}", token),
			}
		}

		board.en_passant = Self::position_to_bitboard(arrays[3]);

		board.halfmove_clock = arrays[4].parse::<u8>().unwrap();

		board.fullmove_number = arrays[5].parse::<u16>().unwrap();

		board
	}

	fn board_to_fen(&self) -> String {
		let mut output = String::with_capacity(66);
		output
	}

	fn position_to_bitboard(position: &str) -> u64 {
		if position == "-" {
			return 0;
		}
	    let file = position.chars().nth(0).unwrap() as u8 - b'a';
	    let rank = position.chars().nth(1).unwrap() as u8 - b'1';
	
	    if file > 7 || rank > 7 {
			panic!("Invalid position");
		}

	    1u64 << (7 - file + rank * 8)
	}

	pub fn piece_on(&self, square: i32) -> char {
		let player = self.color_pieces[0] as u128 & (1u128 << square as u128) != 0;
		for i in 0..6 {
			if self.pieces[i] as u128 & (1u128 << square as u128) != 0 {
				return Chars[i | (player as usize) << 3];
			}
		}
		' '
	}

	fn print_bitboard(bitboard: &Bitboard) -> String {
		let mut output = String::with_capacity(613);
		for rank in (0..8).rev() {
			output.push_str("\n+---+---+---+---+---+---+---+---+\n|");
			for file in (0..8).rev() {
				output.push_str(&format!(" {} |", bitboard.piece_on(rank * 8 + file)));
			}
		}
		output.push_str("\n+---+---+---+---+---+---+---+---+\n");
		output.push_str("  a   b   c   d   e   f   g   h  \n");
		output
	}

	/*
	position ^ (position - 1) gets only least significant bit
	need to turn that into an index to an array
	position.trailing_zeros() gets number of 0s
	lookup table for conversion to 

	*/

	fn generate_king_moves(&self) -> u64 {
		let position = self.color_pieces[!self.white_turn as usize] & self.pieces[King];
		let index = position.trailing_zeros() as usize;
		if index > 63 {
			return 0;
		}
		KING_MOVES[index] & !self.color_pieces[!self.white_turn as usize]
	}

	fn generate_knight_moves(&self) -> u64 {
		let mut position = self.color_pieces[!self.white_turn as usize] & self.pieces[Knight];
		let mut moves = 0;
		while position != 0 {
			moves |= KNIGHT_MOVES[position.trailing_zeros() as usize] & !self.color_pieces[!self.white_turn as usize];
			position = position & (position - 1);
		}
		moves
	}

	fn generate_pawn_moves(&self) -> u64 {
		let mut position = self.color_pieces[!self.white_turn as usize] & self.pieces[Pawn];
		let mut moves = if self.white_turn { (position & 0xFF00) << 16 } else { (position & 0xFF00000000000000) >> 16 } & !self.color_pieces[0] & !self.color_pieces[1] | if self.white_turn { position << 8 } else { position >> 8 } & !self.color_pieces[0] & !self.color_pieces[1];
		while position != 0 {
			let index = position.trailing_zeros() as usize;
			moves |= PAWN_MOVES[!self.white_turn as usize][index] & !self.color_pieces[!self.white_turn as usize] & self.color_pieces[self.white_turn as usize];
			position &= position - 1;
		}
		moves
	}

	pub fn generate_moves(&self, quiet_moves: bool) {
//		let temp = self.generate_king_moves();
//		let temp = self.generate_knight_moves();
		let temp = self.generate_pawn_moves();
		println!("{}", temp);
	}
}

/*
impl FromStr for Bitboard {
	type Err = ();
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Ok(Self::from_fen(s)),
	}
}
*/

impl Display for Bitboard {
	fn fmt(&self, f: &mut Formatter<'_>) -> Result {
		let temp = Bitboard::print_bitboard(self);
		write!(f, "{}", temp)
	}
}

fn main() {
	let positions = include_str!("src/test_data/valid.sfens");
	for fen in positions.lines() {
		let board = Bitboard::from_fen(fen);
		print!("{}", board);
		let temp = board.generate_moves(true);
	}
}